# POST LIST

Post List es un proyecto desarrollado en Swift, para iPhone. 
Consume el api de jsonplaceholder.typicode.com para mostrar una lista de post con su respectivo detalle

Para comenzar a ejecutar el proyecto, descárguelo y ejecute "pod install" para instalar todas las dependencias

# Configuración

Se implementa el patrón de diseño VIPER. 
Se crean vistas independientes para las celdas del TableView.
Se usan extensiones para crear estilos para textos y botones.
Se utiliza la librería Alamofire para la conexión con el api.
Se crearon pruebas de integración y pruebas unitarias de los presenter. 

# Por qué Alamofire?

Considero que Alamorife es una de las librerías escritas en swift con mayor actualización y soporte, buena documentación y fácil de usar. Permite realizar la conexión a un API sin la necesidad de preocuparse por temas de concurrencia.

- Desventajas de Alamofire: Como esta librería está en constante actualización, te obliga aumentar las versiones de OS soportadas en tu aplicación cada vez que la librería las aumenta. También puede cambiar su sintaxis, lo que conlleva a hacer una actualización en el código de la aplicación cada vez que esto ocurra.

