//
//  ServiceManager.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import Alamofire

class ServiceManager {
    
    private let method: HTTPMethod
    private let parameters: [String: Any]?
    private let endPoint: EndPoint
    
    init(method: HTTPMethod = .get, endPoint: EndPoint, parameters: [String: Any]? = nil) {
        self.method = method
        self.parameters = parameters
        self.endPoint = endPoint
    }
    
    func execute<T: Decodable>(_ completion: @escaping (Result<T, NSError>) -> Void) {
        guard let url = URL(string: self.endPoint.url) else { return }
        
        AF.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default).responseData {
            response in
            
            switch response.result {
            case .success(let data):
                do {
                    let object = try JSONDecoder().decode(T.self, from: data)
                    completion(Result.success(object))
                } catch {
                    let nsError = NSError(
                        domain: "Parse error in \(self.endPoint.url)",
                        code: self.endPoint.errorCode,
                        userInfo: ["message" : messages.error]
                    )
                    
                    completion(Result.failure(nsError))
                }
            case .failure(let error):
                completion(Result.failure(error as NSError))
            }
        }
    }
}
