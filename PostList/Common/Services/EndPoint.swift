//
//  EndPoint.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import Foundation

private let pathBase: String = "https://jsonplaceholder.typicode.com/"

enum EndPoint {
    case getAllPosts
    case getUser(_ id: Int)
    case getComments(postId: Int)
    
    var url: String {
        let complement: String
        switch self {
        case .getAllPosts:
            complement = "posts"
        case .getUser(let id):
            complement = "users/\(id)"
        case .getComments(let postId):
            complement = "posts/\(postId)/comments"
        }
        
        return pathBase + complement
    }
    
    var errorCode: Int {
        switch self {
        case .getAllPosts:
            return 100
        case .getUser:
            return 101
        case .getComments:
            return 102
        }
    }
}
