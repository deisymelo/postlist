//
//  UILabel.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import UIKit

extension UILabel {
    
    func titleStyle() {
        font = UIFont(name: fontType.bold, size: textSize.title)
        textColor = UIColor.text
    }
    
    func boldStyle() {
        font = UIFont(name: fontType.bold, size: textSize.bold)
        textColor = UIColor.text
    }
    
    func normalStyle() {
        font = UIFont(name: fontType.regular, size: textSize.normal)
        textColor = UIColor.text
    }
    
    func bylineStyle() {
        font = UIFont(name: fontType.regular, size: textSize.byline)
        textColor = UIColor.textSecundary
    }
    
    func bylineBoldStyle() {
        font = UIFont(name: fontType.bold, size: textSize.byline)
        textColor = UIColor.textSecundary
    }
    
    func linkStyle() {
        font = UIFont(name: fontType.bold, size: textSize.normal)
        textColor = UIColor.accent
    }
}
