//
//  UIButton.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import UIKit

extension UIButton {
    func deleteStyle(title: String) {
        
        if #available(iOS 15.0, *) {
            configuration = .filled()
            configuration?.baseBackgroundColor = UIColor.red
            configuration?.baseForegroundColor = UIColor.white
            configuration?.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 8)
            configurationUpdateHandler = { button in
                var container = AttributeContainer()
                container.font = UIFont(name: fontType.bold, size: textSize.bold)
                button.configuration?.attributedTitle = AttributedString(title, attributes: container)
            }
        } else {
            setTitle(title, for: .normal)
            backgroundColor = UIColor.red
            titleLabel?.font = UIFont(name: fontType.bold, size: textSize.bold)
            setTitleColor(UIColor.white, for: .normal)
            contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        }
    }
}
