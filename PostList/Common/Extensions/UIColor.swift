//
//  UIColor.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import UIKit

extension UIColor {
    
    class var primary: UIColor? {
        return UIColor(named: "primary")
    }
    
    class var text: UIColor? {
        return UIColor(named: "text")
    }
    
    class var textSecundary: UIColor? {
        return UIColor(named: "textSecundary")
    }
    
    class var accent: UIColor? {
        return UIColor(named: "accent")
    }
    
    class var darkGray: UIColor? {
        return UIColor(named: "darkGray")
    }
    
    class var gray: UIColor? {
        return UIColor(named: "gray")
    }
    
    class var lightGray: UIColor? {
        return UIColor(named: "lightGray")
    }
    
    class var background: UIColor? {
        return UIColor(named: "background")
    }
}
