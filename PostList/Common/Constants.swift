//
//  Constants.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import UIKit

struct fontType {
    static let regular = "Montserrat-Regular"
    static let bold = "Montserrat-SemiBold"
    static let light = "Montserrat-Light"
}

struct textSize {
    static let title: CGFloat = 25
    static let bold: CGFloat = 16
    static let normal: CGFloat =  15
    static let byline: CGFloat = 13
}

struct messages {
    static let internet = NSLocalizedString("offline", comment: "")
    static let error = NSLocalizedString("error", comment: "")
    static let loading = NSLocalizedString("loading", comment: "")
    static let understood = NSLocalizedString("understood", comment: "")
}

let kEmptyString: String = ""
