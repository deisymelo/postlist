//
//  DataBaseManager.swift
//  PostList
//
//  Created by Deisy Melo on 15/01/22.
//

import UIKit
import CoreData

class DataBaseManager {
    
    static let sharedInstance = DataBaseManager()
    
    private let appDelegate =  UIApplication.shared.delegate as! AppDelegate
    private let postEntity: String = "PostEntity"
    
    func getAllPost() -> [Post] {
        var list: [Post] = []
        
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: postEntity)
        
        do {
            guard let result = try? context.fetch(fetchRequest),
                  !result.isEmpty else {
                return []
            }
            
            result.forEach({ data in
                list.append(
                    Post(
                        id: data.value(forKey: "id") as? Int ?? 0,
                        userId: data.value(forKey: "userId") as? Int ?? 0,
                        title: data.value(forKey: "title") as? String ?? kEmptyString,
                        body: data.value(forKey: "body") as? String ?? kEmptyString,
                        isNew: data.value(forKey: "isNew") as? Bool ?? false,
                        isFavorite: data.value(forKey: "isFavorite") as? Bool ?? false
                    )
                )
            })
            
            return list
        }
    }
    
    func savePosts(_ posts: [Post]) {
        let context = appDelegate.persistentContainer.viewContext
        
        guard let entity = NSEntityDescription.entity(forEntityName: postEntity, in: context)  else { return }
        
        let fetch = NSFetchRequest<NSManagedObject>(entityName: postEntity)
        let result = try? context.fetch(fetch)
        result?.forEach({ item in
            context.delete(item)
        })
        
        posts.forEach { post in
            let newPost = NSManagedObject(entity: entity, insertInto: context)
            newPost.setValue(post.id, forKey: "id")
            newPost.setValue(post.userId, forKey: "userId")
            newPost.setValue(post.title, forKey: "title")
            newPost.setValue(post.body, forKey: "body")
            newPost.setValue(post.isNew, forKey: "isNew")
            newPost.setValue(post.isFavorite, forKey: "isFavorite")
        }
        
        do {
            try? context.save()
        }
    }
    
    func updateStatus(of post: Post, isNew: Bool) {
        let context = appDelegate.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSManagedObject>(entityName: postEntity)
        let result = try? context.fetch(fetch)
        
        if let item = result?.first(where: { $0.value(forKey: "id") as? Int ?? 0 == post.id }) {
            do {
                item.setValue(isNew, forKey: "isNew")
                try? context.save()
            }
        }
    }
    
    func updateStatus(of post: Post, isFavorite: Bool) {
        let context = appDelegate.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSManagedObject>(entityName: postEntity)
        let result = try? context.fetch(fetch)
        
        if let item = result?.first(where: { $0.value(forKey: "id") as? Int ?? 0 == post.id }) {
            do {
                item.setValue(isFavorite, forKey: "isFavorite")
                try? context.save()
            }
        }
    }
    
    func deletePost(_ id: Int) {
        let context = appDelegate.persistentContainer.viewContext
        
        let fetch = NSFetchRequest<NSManagedObject>(entityName: postEntity)
        let result = try? context.fetch(fetch)
        
        if let item = result?.first(where: { $0.value(forKey: "id") as? Int ?? 0 == id }) {
            do {
                context.delete(item)
                try? context.save()
            }
        }
    }
    
    
    func deleteAllPosts() {
        let context = appDelegate.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSManagedObject>(entityName: postEntity)
        let result = try? context.fetch(fetch)
        result?.forEach({ item in
            context.delete(item)
        })
        
        do {
            try? context.save()
        }
    }
}
