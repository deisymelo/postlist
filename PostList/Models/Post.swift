//
//  Post.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import Foundation

struct Post: Decodable {
    var id: Int
    var userId: Int
    var title: String
    var body: String
    var isNew: Bool?
    var isFavorite: Bool?
}
