//
//  User.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import Foundation

struct User: Decodable {
    var id: Int
    var name: String
    var userName: String
    var email: String
    var phone: String
    var website: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case userName = "username"
        case email
        case phone
        case website
    }
}
