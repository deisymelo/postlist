//
//  MainInterfaces.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import Foundation

enum MainLocalizable: String {
    case allOption = "main.allOption"
    case deleteButton = "main.deleteButton"
    case favoritesOption = "main.favoritesOption"
    case sorry = "main.sorry"
    case emptyStateFavorites = "main.emptyStateFavorites"
    case emptyState = "main.emptyState"
    case titleView = "main.titleView"
    case deleteItemButton = "main.deleteItemButton"
}

enum MainSegmentedOptions: Int, CaseIterable {
    case all
    case favorites
}

enum GetAllPostResult {
    case success([Post])
    case error(String)
}

protocol MainWireframeInterface: AnyObject {
    func showPostDetail(_ post: Post, from view: MainViewInterface)
}

protocol MainViewInterface: AnyObject {
    func changeLoader(isLoader: Bool)
    func realoadData()
    func emptyState(isHidden: Bool)
    func setEmptyState(title: String, message: String)
}

protocol MainPresenterInterface {
    var numberOfRows: Int { get }
    func getLocalizable(_ text: MainLocalizable) -> String
    func getItemForRow(at indexPath: IndexPath) -> PostCellViewModel?
    func getAllPosts()
    func didSelectRow(at index: IndexPath)
    func didPressReload()
    func deleteAllOnAction()
    func selectedSegment(_ index: Int)
    func deletePost(_ index: IndexPath)
}

protocol MainInteractorInterface {
    var allPostList: [Post] { get }
    var favoritesList: [Post] { get }
    func getAllPosts(completion: @escaping (GetAllPostResult) -> Void)
    func updateStatus(of index: Int, isNew: Bool)
    func deleteAllPost()
    func deletePost(_ index: Int, section: MainSegmentedOptions)
    func getPost(by index: Int, section: MainSegmentedOptions) -> Post?
}
