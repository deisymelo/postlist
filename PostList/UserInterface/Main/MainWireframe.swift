//
//  MainWireframe.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import UIKit

final class MainWireframe {
    
    func createModule() -> UINavigationController {
        let viewController = MainViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let interactor = MainInteractor()
        let presenter = MainPresenter(wireframe: MainWireframe(), interactor: interactor, view: viewController)
        viewController.presenter = presenter
        
        return navigationController
    }
}
    
extension MainWireframe: MainWireframeInterface {
    
    func showPostDetail(_ post: Post, from view: MainViewInterface) {
        guard let viewMain = view as? MainViewController else { return }
        
        let productDetailView = DetailWireframe().createModule(post)
        viewMain.navigationController?.pushViewController(productDetailView, animated: true)
    }
}
