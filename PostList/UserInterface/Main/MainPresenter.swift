//
//  MainPresenter.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import Foundation

final class MainPresenter {
    
    private unowned var view: MainViewInterface
    private var interactor: MainInteractorInterface
    private var wireframe: MainWireframeInterface
    private var segmentedOptionSelected: MainSegmentedOptions = .all
    
    init(
        wireframe: MainWireframeInterface,
        interactor: MainInteractorInterface,
        view: MainViewInterface
    ) {
        self.wireframe = wireframe
        self.interactor = interactor
        self.view = view
    }
    
    private func displayData() {
        var listIsEmpty: Bool = false
        switch segmentedOptionSelected {
        case .all:
            if interactor.allPostList.isEmpty {
                listIsEmpty = true
                view.setEmptyState(
                    title: getLocalizable(.sorry),
                    message: getLocalizable(.emptyState)
                )
            }
        case .favorites:
            if interactor.favoritesList.isEmpty {
                listIsEmpty = true
                view.setEmptyState(
                    title: getLocalizable(.sorry),
                    message: getLocalizable(.emptyStateFavorites)
                )
            }
        }
        
        view.emptyState(isHidden: !listIsEmpty)
        view.realoadData()
    }
}

extension MainPresenter: MainPresenterInterface {
    
    var numberOfRows: Int {
        switch segmentedOptionSelected {
        case .all: return interactor.allPostList.count
        case .favorites: return interactor.favoritesList.count
        }
    }
    
    func getLocalizable(_ text: MainLocalizable) -> String {
        return NSLocalizedString(text.rawValue, comment: "")
    }
    
    func getItemForRow(at indexPath: IndexPath) -> PostCellViewModel? {
        let item: Post
        
        switch segmentedOptionSelected {
        case .all: item = interactor.allPostList[indexPath.row]
        case .favorites: item = interactor.favoritesList[indexPath.row]
        }
        
        return PostCellViewModel(
            body: item.body,
            isNew: item.isNew ?? false,
            isFavorite: item.isFavorite ?? false
        )
    }
    
    func getAllPosts() {
        segmentedOptionSelected = .all
        view.changeLoader(isLoader: true)
        
        interactor.getAllPosts { [weak self] result in
            guard let strongSelf = self else { return }
            
            strongSelf.view.changeLoader(isLoader: false)

            switch result {
            case .success:
                strongSelf.displayData()
            case .error(let error):
                strongSelf.view.setEmptyState(
                    title: strongSelf.getLocalizable(.sorry),
                    message: error
                )
                strongSelf.view.emptyState(isHidden: false)
            }
        }
    }
        
    func selectedSegment(_ index: Int) {
        guard let option = MainSegmentedOptions(rawValue: index) else {
            return
        }
        
        segmentedOptionSelected = option
        displayData()
    }
    
    func didSelectRow(at index: IndexPath) {
        guard let post = interactor.getPost(by: index.row, section: segmentedOptionSelected) else {
            return
        }
        
        if post.isNew ?? false { interactor.updateStatus(of: index.row, isNew: false) }
        wireframe.showPostDetail(post, from: view)
    }
    
    func didPressReload() {
        getAllPosts()
    }
    
    func deleteAllOnAction() {
        interactor.deleteAllPost()
        displayData()
    }
    
    func deletePost(_ index: IndexPath) {
        interactor.deletePost(index.row, section: segmentedOptionSelected)
    }
}
