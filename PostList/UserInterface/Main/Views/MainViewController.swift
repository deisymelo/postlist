//
//  MainViewController.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import UIKit

class MainViewController: UIViewController {
    
    // MARK: - Properties -
    @IBOutlet private weak var contentSegmented: UIView!
    @IBOutlet private weak var deleteButton: UIButton!
    @IBOutlet private weak var emptyStateContent: UIView!
    @IBOutlet private weak var emptyStateTitleLabel: UILabel!
    @IBOutlet private weak var emptyStateDetailLabel: UILabel!
    @IBOutlet private weak var loader: UIActivityIndicatorView!
    @IBOutlet private weak var postTable: UITableView!
    @IBOutlet private weak var segmentedControl: UISegmentedControl!
    
    var presenter: MainPresenterInterface!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.getAllPosts()
        segmentedControl.selectedSegmentIndex = MainSegmentedOptions.all.rawValue
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.background
        loader.hidesWhenStopped = true
        deleteButton.isHidden = true
        emptyStateContent.isHidden = true
        emptyStateTitleLabel.boldStyle()
        emptyStateDetailLabel.bylineStyle()
        emptyStateDetailLabel.numberOfLines = 0
        setupNavigationBar()
        setupTable()
        setupSegmentedControl()
        contentSegmented.backgroundColor = UIColor.background
        deleteButton.deleteStyle(title: presenter.getLocalizable(.deleteButton))
    }
    
    private func setupNavigationBar() {
        navigationItem.title = presenter.getLocalizable(.titleView)
        navigationItem.backButtonTitle = kEmptyString
        
        let buttonReload = UIImage(named: "ic_reload")!.withRenderingMode(.alwaysOriginal)
        
        let optionsButton = UIBarButtonItem(
            image: buttonReload,
            style: .done, target: self,
            action: #selector(self.didPressReload)
        )
        
        self.navigationItem.rightBarButtonItem = optionsButton
    }
    
    private func setupTable() {
        postTable.register(
            UINib(nibName: PostCell.cellName, bundle: nil),
            forCellReuseIdentifier: PostCell.cellName
        )
        
        postTable.backgroundColor = UIColor.background
        postTable.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        postTable.separatorColor = UIColor.darkGray
        postTable.rowHeight = UITableView.automaticDimension
        postTable.tableFooterView = UIView()
        postTable.showsVerticalScrollIndicator = false
        postTable.delegate = self
        postTable.dataSource = self
    }
    
    private func setupSegmentedControl() {
        segmentedControl.selectedSegmentTintColor = UIColor.primary
        segmentedControl.backgroundColor = UIColor.white
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.textSecundary]
        segmentedControl.setTitleTextAttributes(titleTextAttributes as [NSAttributedString.Key : Any], for: .normal)
        segmentedControl.setTitleTextAttributes(titleTextAttributes as [NSAttributedString.Key : Any], for: .selected)
        
        segmentedControl.setTitle(
            presenter.getLocalizable(.allOption),
            forSegmentAt: MainSegmentedOptions.all.rawValue
        )
        segmentedControl.setTitle(
            presenter.getLocalizable(.favoritesOption),
            forSegmentAt: MainSegmentedOptions.favorites.rawValue
        )
    }
    
    @objc private func didPressReload() {
        segmentedControl.selectedSegmentIndex = MainSegmentedOptions.all.rawValue
        presenter.didPressReload()
    }
    
    @IBAction func deleteAllOnAction(_ sender: UIButton) {
        presenter.deleteAllOnAction()
    }
    
    @IBAction func selectOptionOnAction(_ sender: UISegmentedControl) {
        presenter.selectedSegment(sender.selectedSegmentIndex)
    }
}

// MARK: - UITableView DataSource -
extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reusableCell = tableView.dequeueReusableCell(withIdentifier: PostCell.cellName, for: indexPath)
        
        guard let cell = reusableCell as? PostCell,
              let viewModel = presenter.getItemForRow(at: indexPath) else {
            return UITableViewCell()
        }
        
        cell.setData(viewModel)
        return cell
    }
}

// MARK: - UITableView Delegate -
extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: true)
        presenter.didSelectRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteButton = UIContextualAction(style: .normal, title: kEmptyString) { (_, _, completion: @escaping (Bool) -> Void) in
            self.presenter.deletePost(indexPath)
            tableView.deleteRows(at: [indexPath], with: .left)
            tableView.reloadData()
            completion(true)
        }
        deleteButton.title = presenter.getLocalizable(.deleteItemButton)
        deleteButton.backgroundColor = UIColor.red
        return UISwipeActionsConfiguration(actions: [deleteButton])
    }
}

// MARK: - MainView Interface -
extension MainViewController: MainViewInterface {
    
    func changeLoader(isLoader: Bool) {
        isLoader ? loader.startAnimating() : loader.stopAnimating()
    }
    
    func realoadData() {
        postTable.reloadData()
    }

    func emptyState(isHidden: Bool) {
        postTable.isHidden = !isHidden
        deleteButton.isHidden = !isHidden
        emptyStateContent.isHidden = isHidden
    }
    
    func setEmptyState(title: String, message: String) {
        emptyStateTitleLabel.text = title
        emptyStateDetailLabel.text = message
    }
}
