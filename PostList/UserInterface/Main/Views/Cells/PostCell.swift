//
//  PostCell.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import UIKit

struct PostCellViewModel {
    var body: String
    var isNew: Bool
    var isFavorite: Bool
}

class PostCell: UITableViewCell {
    
    @IBOutlet private weak var bodyLabel: UILabel!
    @IBOutlet private weak var chevronImage: UIImageView!
    @IBOutlet private weak var unreadView: UIView!
    @IBOutlet private weak var favoriteImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override func prepareForReuse() {
        unreadView.isHidden = true
        favoriteImage.isHidden = true
        bodyLabel.text = kEmptyString
    }
    
    private func setupView() {
        unreadView.layer.cornerRadius = unreadView.bounds.height / 2
        unreadView.backgroundColor = UIColor.accent
        unreadView.isHidden = true
        favoriteImage.isHidden = true
        bodyLabel.text = kEmptyString
        bodyLabel.normalStyle()
        bodyLabel.numberOfLines = 0
        contentView.backgroundColor = UIColor.background
    }
    
    func setData(_ data: PostCellViewModel) {
        bodyLabel.text = data.body
        favoriteImage.isHidden = !data.isFavorite
        unreadView.isHidden = !data.isNew
    }
}

extension PostCell {
    static let cellName = "PostCell"
}
