//
//  MainInteractor.swift
//  PostList
//
//  Created by Deisy Melo on 14/01/22.
//

import Foundation

final class MainInteractor {
    private let dataBaseManager: DataBaseManager = DataBaseManager.sharedInstance
    var allPostList: [Post] = []
}

extension MainInteractor: MainInteractorInterface {
    
    var favoritesList: [Post] { allPostList.filter { $0.isFavorite == true } }
    
    func getAllPosts(completion: @escaping (GetAllPostResult) -> Void) {
        let list = dataBaseManager.getAllPost().sorted { $0.id < $1.id }
        
        guard list.isEmpty else {
            allPostList.removeAll()
            allPostList.append(contentsOf: list)
            completion(.success(self.allPostList))
            return
        }
        
        ServiceManager(endPoint: .getAllPosts).execute { (response: Result<[Post], NSError>) in
            switch response {
            case .success(let result):
                self.allPostList.append(contentsOf: result)
                
                if !result.isEmpty {
                    let range = result.count > 20 ? 0...19 : 0...(result.count - 1)
                    
                    for index in range {
                        self.allPostList[index].isNew = true
                    }
                }
                
                self.dataBaseManager.savePosts(self.allPostList)
                completion(.success(self.allPostList))
            case .failure(let error):
                completion(.error(error.localizedDescription))
            }
        }
    }
    
    func updateStatus(of index: Int, isNew: Bool) {
        allPostList[index].isNew = isNew
        dataBaseManager.updateStatus(of: allPostList[index], isNew: isNew)
    }
    
    func deleteAllPost() {
        allPostList.removeAll()
        dataBaseManager.deleteAllPosts()
    }
    
    func deletePost(_ index: Int, section: MainSegmentedOptions) {
        let post: Post?
        
        switch section {
        case .all: post = allPostList.remove(at: index)
        case .favorites:
            let favoriteId = favoritesList[index].id
            guard let indexPost = allPostList.firstIndex(where: { $0.id == favoriteId }) else {
                return
            }
            post = allPostList.remove(at: indexPost)
        }
        
        if let item = post {
            dataBaseManager.deletePost(item.id)
        }
    }
    
    func getPost(by index: Int, section: MainSegmentedOptions) -> Post? {
        switch section {
        case .all: return allPostList[index]
        case .favorites: return favoritesList[index]
        }
    }
}
