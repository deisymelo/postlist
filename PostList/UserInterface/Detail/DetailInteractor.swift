//
//  DetailInteractor.swift
//  PostList
//
//  Created by Deisy Melo on 15/01/22.
//

import Foundation

final class DetailInteractor {
    
    private let servicesGroup = DispatchGroup()
    private let dataBaseManager: DataBaseManager = DataBaseManager.sharedInstance
    private var errorMessage: String?
    
    var post: Post
    var user: User?
    var comments: [Comment]?
    
    init(_ post: Post) {
        self.post = post
    }
    
    private func getPostUser() {
        servicesGroup.enter()
        ServiceManager(endPoint: .getUser(post.userId)).execute { [weak self] (response: Result<User, NSError>) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                strongSelf.user = result
            case .failure(let error):
                strongSelf.errorMessage = error.localizedDescription
            }
            strongSelf.servicesGroup.leave()
        }
    }
    
    private func getPostComments() {
        servicesGroup.enter()
        ServiceManager(endPoint: .getComments(postId: post.id)).execute { [weak self] (response: Result<[Comment], NSError>) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let result):
                strongSelf.comments = result
            case .failure(let error):
                strongSelf.errorMessage = error.localizedDescription
            }
            strongSelf.servicesGroup.leave()
        }
    }
}

extension DetailInteractor: DetailInteractorInterface {
    
    func getPostDetail(completion: @escaping (PostDetailResult) -> Void) {
        getPostUser()
        getPostComments()
        
        servicesGroup.notify(queue: DispatchQueue.main) { [weak self] in
            guard let strongSelf = self else { return }
            guard let message = strongSelf.errorMessage else {
                completion(.success)
                return
            }
            
            completion(.error(message))
            
        }
    }
    
    func updateFavoriteStatus() {
        dataBaseManager.updateStatus(of: post, isFavorite: !(post.isFavorite ?? false))
        post.isFavorite = !(post.isFavorite ?? false)
    }
    
}
