//
//  DetailInterfaces.swift
//  PostList
//
//  Created by Deisy Melo on 15/01/22.
//

import Foundation

enum DetailLocalizable: String {
    case titleView = "detail.titleView"
    case commentsSection = "detail.commentsSection"
}

enum DetailSections: Int, CaseIterable {
    case userInfo
    case post
    case comments
}

enum DetailTableItem {
    case userInfo(UserCellModel)
    case post(PostDetailCellModel)
    case comment(CommentCellModel)
}

enum PostDetailResult {
    case success
    case error(String)
}

protocol DetailWireframeInterface: AnyObject {
}

protocol DetailViewInterface: AnyObject {
    func changeLoader(isLoader: Bool)
    func realoadData()
    func updateFavoriteState(_ isFavorite: Bool)
}

protocol DetailPresenterInterface {
    func getLocalizable(_ text: DetailLocalizable) -> String
    func getNumberOfRows(in section: Int) -> Int
    func titleOfSection(in section: Int) -> String
    func getItemForRow(at indexPath: IndexPath) -> DetailTableItem?
    func getData()
    func changeFavoriteStatus() 
}

protocol DetailInteractorInterface {
    var post: Post { get }
    var user: User? { get }
    var comments: [Comment]? { get }
    func getPostDetail(completion: @escaping (PostDetailResult) -> Void)
    func updateFavoriteStatus()
}
