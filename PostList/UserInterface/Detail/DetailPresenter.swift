//
//  DetailPresenter.swift
//  PostList
//
//  Created by Deisy Melo on 15/01/22.
//

import Foundation

final class DetailPresenter {
    
    private unowned var view: DetailViewInterface
    private var interactor: DetailInteractorInterface
    private var wireframe: DetailWireframeInterface
    
    init(
        wireframe: DetailWireframeInterface,
        interactor: DetailInteractorInterface,
        view: DetailViewInterface
    ) {
        self.wireframe = wireframe
        self.interactor = interactor
        self.view = view
    }
}

extension DetailPresenter: DetailPresenterInterface {
    
    func getLocalizable(_ text: DetailLocalizable) -> String {
        return NSLocalizedString(text.rawValue, comment: "")
    }
    
    func getNumberOfRows(in section: Int) -> Int {
        switch DetailSections(rawValue: section) {
        case .userInfo: return interactor.user != nil ? 1 : 0
        case .post: return 1
        case .comments: return interactor.comments?.count ?? 0
        default: return 0
        }
    }
    
    func titleOfSection(in section: Int) -> String {
        switch DetailSections(rawValue: section) {
        case .comments: return getLocalizable(.commentsSection)
        default: return kEmptyString
        }
    }
    
    func getItemForRow(at indexPath: IndexPath) -> DetailTableItem? {
        switch DetailSections(rawValue: indexPath.section) {
        case .userInfo:
            guard let user = interactor.user else { return nil }
            let model = UserCellModel(
                name: user.name,
                userName: user.userName,
                email: user.email,
                webSite: user.website
            )
            return .userInfo(model)
        case .post:
            let post = interactor.post
            let model = PostDetailCellModel(title: post.title, body: post.body)
            return .post(model)
        case .comments:
            guard let comment = interactor.comments?[indexPath.row] else { return nil }
            let model = CommentCellModel(comment: comment.body)
            return .comment(model)
        default: return nil
        }
    }
    
    func getData() {
        view.updateFavoriteState(interactor.post.isFavorite ?? false)
        view.changeLoader(isLoader: true)
        interactor.getPostDetail { [ weak self ] result in
            guard let strongSelf = self else { return }
            strongSelf.view.changeLoader(isLoader: false)
            strongSelf.view.realoadData()
            
        }
    }
    
    func changeFavoriteStatus() {
        interactor.updateFavoriteStatus()
        view.updateFavoriteState(interactor.post.isFavorite ?? false)
    }
}

