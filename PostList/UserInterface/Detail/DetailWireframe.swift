//
//  DetailWireframe.swift
//  PostList
//
//  Created by Deisy Melo on 15/01/22.
//

import UIKit

final class DetailWireframe {
    
    func createModule(_ post: Post) -> UIViewController {
        let viewController = DetailViewController()
        
        let interactor = DetailInteractor(post)
        let presenter = DetailPresenter(wireframe: DetailWireframe(), interactor: interactor, view: viewController)
        viewController.presenter = presenter
        
        return viewController
    }
}
    
extension DetailWireframe: DetailWireframeInterface {

}
