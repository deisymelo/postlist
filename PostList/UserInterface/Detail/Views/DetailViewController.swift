//
//  DetailViewController.swift
//  PostList
//
//  Created by Deisy Melo on 15/01/22.
//

import UIKit

class DetailViewController: UIViewController {
    
    // MARK: - Properties -
    private lazy var loader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView()
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.hidesWhenStopped = true
        loader.style = .medium
        return loader
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.background
        return view
    }()
    
    private lazy var detailsTable: UITableView = {
        let table = UITableView(frame: .zero, style: .grouped)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.backgroundColor = UIColor.background
        table.rowHeight = UITableView.automaticDimension
        table.showsVerticalScrollIndicator = false
        table.sectionFooterHeight = 0
        table.tableFooterView = UIView()
        table.delegate = self
        table.dataSource = self
        table.register(UserCell.self, forCellReuseIdentifier: UserCell.cellName)
        table.register(PostDetailCell.self, forCellReuseIdentifier: PostDetailCell.cellName)
        table.register(CommentCell.self, forCellReuseIdentifier: CommentCell.cellName)
        return table
    }()

    var presenter: DetailPresenterInterface!
    
    // MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        addViews()
        setConstraint()
        setupNavigationBar()
        presenter.getData()
        view.backgroundColor = UIColor.background
    }
    
    // MARK: - Private methods -
    private func addViews() {
        contentView.addSubview(detailsTable)
        contentView.addSubview(loader)
        view.addSubview(contentView)
    }
    
    private func setConstraint() {
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            detailsTable.topAnchor.constraint(equalTo: contentView.topAnchor),
            detailsTable.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            detailsTable.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            detailsTable.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),

            loader.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            loader.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
        ])
    }
    
    private func setupNavigationBar() {
        navigationItem.title = presenter.getLocalizable(.titleView)
        navigationItem.backButtonTitle = kEmptyString
        
        let buttonReload = UIImage(named: "ic_star")!.withRenderingMode(.alwaysOriginal)
        
        let optionsButton = UIBarButtonItem(
            image: buttonReload,
            style: .done, target: self,
            action: #selector(self.didPressFavorite)
        )
        
        self.navigationItem.rightBarButtonItem = optionsButton
    }
    
    private func getHeaderSectionView(section: Int) -> UIView? {
        let viewHeader = UIView()
        let titleHeader = UILabel()
        let margin: CGFloat = 16
        viewHeader.addSubview(titleHeader)
        
        titleHeader.translatesAutoresizingMaskIntoConstraints = false
        titleHeader.leftAnchor.constraint(equalTo: viewHeader.leftAnchor, constant: margin).isActive = true
        titleHeader.rightAnchor.constraint(equalTo: viewHeader.rightAnchor, constant: -margin).isActive = true
        titleHeader.centerYAnchor.constraint(equalTo: viewHeader.centerYAnchor, constant: 0.0).isActive = true
        
        viewHeader.backgroundColor = UIColor.textSecundary
        titleHeader.boldStyle()
        titleHeader.textColor = UIColor.white
        titleHeader.numberOfLines = 0

        titleHeader.text = presenter.titleOfSection(in: section)
        
        return viewHeader
    }
    
    @objc private func didPressFavorite() {
        presenter.changeFavoriteStatus()
    }
}

// MARK: - UITableView DataSource -
extension DetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return DetailSections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getNumberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch presenter.getItemForRow(at: indexPath) {
        case .userInfo(let viewModel):
            let reusableCell = tableView.dequeueReusableCell(withIdentifier: UserCell.cellName, for: indexPath)
            
            guard let cell = reusableCell as? UserCell else {
                break
            }
            
            cell.setData(viewModel)
            return cell
        case .post(let viewModel):
            let reusableCell = tableView.dequeueReusableCell(withIdentifier: PostDetailCell.cellName, for: indexPath)
            
            guard let cell = reusableCell as? PostDetailCell else {
                break
            }
            
            cell.setData(viewModel)
            return cell
        case .comment(let viewModel):
            let reusableCell = tableView.dequeueReusableCell(withIdentifier: CommentCell.cellName, for: indexPath)
            
            guard let cell = reusableCell as? CommentCell else {
                break
            }
            
            cell.setData(viewModel)
            return cell
        default:
            break
        }
        
        return UITableViewCell()
    }
}

// MARK: - UITableView Delegate -
extension DetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch DetailSections(rawValue: section) {
        case .comments: return 40
        default: return CGFloat.leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return getHeaderSectionView(section: section)
    }
}

extension DetailViewController: DetailViewInterface {
    
    func changeLoader(isLoader: Bool) {
        isLoader ? loader.startAnimating() : loader.stopAnimating()
    }
    
    func realoadData() {
        detailsTable.reloadData()
    }
    
    func updateFavoriteState(_ isFavorite: Bool) {
        navigationItem.rightBarButtonItem?.image = UIImage(named: isFavorite ? "ic_star_filed" : "ic_star")!.withRenderingMode(.alwaysOriginal)
    }
}
