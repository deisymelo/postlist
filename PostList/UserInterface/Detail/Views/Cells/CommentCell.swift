//
//  CommentCell.swift
//  PostList
//
//  Created by Deisy Melo on 15/01/22.
//

import UIKit

struct CommentCellModel {
    var comment: String
}

class CommentCell: UITableViewCell {
    
    private let commentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.bylineStyle()
        label.numberOfLines = 0
        label.backgroundColor = .clear
        return label
    }()

    // MARK: - Lifecycle -
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        commentLabel.text = kEmptyString
    }
    
    // MARK: - Private methods -
    private func setUpView() {
        addSubviews()
        setConstraints()
        contentView.backgroundColor = UIColor.clear
        selectionStyle = .none
    }
    
    private func addSubviews() {
        contentView.addSubview(commentLabel)
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            commentLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            commentLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            commentLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            commentLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
    }
    
    // MARK: - Public methods -
    func setData(_ data: CommentCellModel) {
        commentLabel.text = data.comment
    }
}

// MARK: - Identifiers -
extension CommentCell {
    static let cellName: String = "CommentCell"
}
