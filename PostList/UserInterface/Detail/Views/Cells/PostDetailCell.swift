//
//  PostDetailCell.swift
//  PostList
//
//  Created by Deisy Melo on 15/01/22.
//

import UIKit

struct PostDetailCellModel {
    var title: String
    var body: String
}

class PostDetailCell: UITableViewCell {
    
    // MARK: - Private properties -
    private let contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.backgroundColor = .clear
        stackView.spacing = 16
        return stackView
    }()
    
    private let contentBody: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.titleStyle()
        label.numberOfLines = 0
        label.backgroundColor = .clear
        return label
    }()
    
    private let bodyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.normalStyle()
        label.backgroundColor = .clear
        label.numberOfLines = 0
        return label
    }()

    // MARK: - Lifecycle -
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = kEmptyString
        bodyLabel.text = kEmptyString
    }
    
    // MARK: - Private methods -
    private func setUpView() {
        addSubviews()
        setConstraints()
        contentView.backgroundColor = UIColor.clear
        selectionStyle = .none
    }
    
    private func addSubviews() {
        contentBody.addSubview(bodyLabel)
        contentStackView.addArrangedSubview(titleLabel)
        contentStackView.addArrangedSubview(contentBody)
        contentView.addSubview(contentStackView)
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            contentStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            contentStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),
            contentStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            contentStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            
            bodyLabel.topAnchor.constraint(equalTo: contentBody.topAnchor),
            bodyLabel.bottomAnchor.constraint(equalTo: contentBody.bottomAnchor),
            bodyLabel.trailingAnchor.constraint(equalTo: contentBody.trailingAnchor),
            bodyLabel.leadingAnchor.constraint(equalTo: contentBody.leadingAnchor)
        ])
    }
    
    // MARK: - Public methods -
    func setData(_ data: PostDetailCellModel) {
        titleLabel.text = data.title
        bodyLabel.text = data.body
    }
}

// MARK: - Identifiers -
extension PostDetailCell {
    static let cellName: String = "PostDetailCell"
}
