//
//  UserCell.swift
//  PostList
//
//  Created by Deisy Melo on 15/01/22.
//

import UIKit

struct UserCellModel {
    var name: String
    var userName: String
    var email: String
    var webSite: String
}

class UserCell: UITableViewCell {
    
    // MARK: - Private properties -
    private let contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.backgroundColor = .clear
        stackView.spacing = 0
        return stackView
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.boldStyle()
        label.backgroundColor = .clear
        return label
    }()
    
    private let emailLabel: UILabel = {
        let label = UILabel()
        label.bylineStyle()
        label.backgroundColor = .clear
        return label
    }()
    
    private let webSiteLabel: UILabel = {
        let label = UILabel()
        label.bylineBoldStyle()
        label.backgroundColor = .clear
        return label
    }()

    // MARK: - Lifecycle -
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = kEmptyString
        emailLabel.text = kEmptyString
        webSiteLabel.text = kEmptyString
    }
    
    // MARK: - Private methods -
    private func setUpView() {
        addSubviews()
        setConstraints()
        contentView.backgroundColor = UIColor.clear
        selectionStyle = .none
    }
    
    private func addSubviews() {
        contentStackView.addArrangedSubview(nameLabel)
        contentStackView.addArrangedSubview(emailLabel)
        contentStackView.addArrangedSubview(webSiteLabel)
        contentView.addSubview(contentStackView)
    }
    
    private func setConstraints() {
        NSLayoutConstraint.activate([
            contentStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            contentStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            contentStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            contentStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
    }
    
    // MARK: - Public methods -
    func setData(_ data: UserCellModel) {
        nameLabel.text = "\(data.name) - \(data.userName)"
        emailLabel.text = data.email
        webSiteLabel.text = data.webSite
    }
}

// MARK: - Identifiers -
extension UserCell {
    static let cellName: String = "UserCell"
}
