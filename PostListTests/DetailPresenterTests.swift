//
//  DetailPresenterTests.swift
//  PostListTests
//
//  Created by Deisy Melo on 16/01/22.
//

import XCTest
@testable import PostList

class DetailPresenterTests: XCTestCase {
    var presenter: DetailPresenter!
    var interactor: TestDetailInteractor!
    var wireframe: TestDetailWireframe!
    var view: TestDetailView!
    
    override func setUp() {
        super.setUp()
        interactor = TestDetailInteractor()
        wireframe = TestDetailWireframe()
        view = TestDetailView()
        presenter = DetailPresenter(wireframe: wireframe, interactor: interactor, view: view)
    }
    
    func testGetAllPosts() {
        presenter.getData()
        XCTAssertEqual(presenter.getNumberOfRows(in: DetailSections.post.rawValue), 1)
        XCTAssertEqual(presenter.getNumberOfRows(in: DetailSections.userInfo.rawValue), 1)
        XCTAssertEqual(presenter.getNumberOfRows(in: DetailSections.comments.rawValue), 2)
        XCTAssertEqual(presenter.titleOfSection(in: DetailSections.comments.rawValue), presenter.getLocalizable(.commentsSection))
        XCTAssertTrue(view.isRealoadData)
        XCTAssertFalse(view.isLoader)
    }
    
    func testChangeFavoriteState() {
        presenter.getData()
        XCTAssertFalse(interactor.post.isFavorite ?? false)
        presenter.changeFavoriteStatus()
        XCTAssertTrue(interactor.post.isFavorite ?? false)
        XCTAssertTrue(view.isFavorite)
    }
}

extension DetailPresenterTests {
    class TestDetailInteractor: DetailInteractorInterface {
        var post: Post = TestConstants.post
        var user: User?
        var comments: [Comment]?
        
        func getPostDetail(completion: @escaping (PostDetailResult) -> Void) {
            guard let userJsonMock = TestConstants.userMock.data(using: .utf8) else {
                completion(.error("data doesn't exits"))
                return
            }
            
            do {
                let userResult = try JSONDecoder().decode(User.self, from: userJsonMock)
                user = userResult
            } catch {
                completion(.error("data doesn't exits"))
            }
            
            guard let commentsJsonMock = TestConstants.commentsMock.data(using: .utf8) else {
                completion(.error("data doesn't exits"))
                return
            }
            
            do {
                let commentsResult = try JSONDecoder().decode([Comment].self, from: commentsJsonMock)
                comments = commentsResult
            } catch {
                completion(.error("data doesn't exits"))
            }
            
            completion(.success)
        }
        
        func updateFavoriteStatus() {
            post.isFavorite = true
        }
    }
    
    class TestDetailWireframe: DetailWireframeInterface {
    }
    
    class TestDetailView: DetailViewInterface {
        var isRealoadData: Bool = false
        var isLoader: Bool = false
        var isFavorite: Bool = false
        func changeLoader(isLoader: Bool) { self.isLoader = isLoader }
        func realoadData() { isRealoadData = true }
        func updateFavoriteState(_ isFavorite: Bool) { self.isFavorite = isFavorite }
    }
}
