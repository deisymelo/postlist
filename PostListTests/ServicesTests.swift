//
//  ServicesTests.swift
//  PostListTests
//
//  Created by Deisy Melo on 14/01/22.
//

import XCTest
@testable import PostList

class ServicesTests: XCTestCase {
    
    func testGetAllPostsService() {
        let serviceExpectation = expectation(description: "Get all posts service instantiation expectation")
        
        ServiceManager(endPoint: .getAllPosts).execute { (response: Result<[Post], NSError>) in
            switch response {
            case .success(let result):
                XCTAssertFalse(result.isEmpty)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            
            serviceExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 3) { error in
            if let error = error {
                XCTFail("exceeded timeout: \(error)")
            }
        }
    }
    
    func testGetUserService() {
        let serviceExpectation = expectation(description: "Get all posts service instantiation expectation")
        
        ServiceManager(endPoint: .getUser(1)).execute { (response: Result<User, NSError>) in
            switch response {
            case .success(let result):
                XCTAssertNotNil(result)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            
            serviceExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 3) { error in
            if let error = error {
                XCTFail("exceeded timeout: \(error)")
            }
        }
    }
    
    func testGetCommentsService() {
        let serviceExpectation = expectation(description: "Get all posts service instantiation expectation")
        
        ServiceManager(endPoint: .getComments(postId: 1)).execute { (response: Result<[Comment], NSError>) in
            switch response {
            case .success(let result):
                XCTAssertFalse(result.isEmpty)
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
            
            serviceExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 3) { error in
            if let error = error {
                XCTFail("exceeded timeout: \(error)")
            }
        }
    }
}
