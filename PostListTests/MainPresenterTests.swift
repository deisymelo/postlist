//
//  MainPresenterTests.swift
//  PostListTests
//
//  Created by Deisy Melo on 15/01/22.
//

import XCTest
@testable import PostList

class MainPresenterTests: XCTestCase {
    var presenter: MainPresenter!
    var interactor: TestMainInteractor!
    var wireframe: TestMainWireframe!
    var view: TestMainView!
    
    override func setUp() {
        super.setUp()
        interactor = TestMainInteractor()
        wireframe = TestMainWireframe()
        view = TestMainView()
        presenter = MainPresenter(wireframe: wireframe, interactor: interactor, view: view)
    }
    
    func testGetAllPosts() {
        presenter.getAllPosts()
        XCTAssertFalse(interactor.allPostList.isEmpty)
        XCTAssertEqual(presenter.numberOfRows, 3)
        XCTAssertTrue(view.isRealoadData)
        XCTAssertFalse(view.isLoader)
        XCTAssertTrue(view.emptyStateIsHidden)
        XCTAssertFalse(view.setUpEmptyState)
    }
    
    func testFavoriteListEmpty() {
        presenter.getAllPosts()
        XCTAssertFalse(interactor.allPostList.isEmpty)
        presenter.selectedSegment(MainSegmentedOptions.favorites.rawValue)
        XCTAssertEqual(presenter.numberOfRows, 0)
    }
    
    func testFavoriteList() {
        presenter.getAllPosts()
        interactor.allPostList[0].isFavorite = true
        presenter.selectedSegment(MainSegmentedOptions.favorites.rawValue)
        XCTAssertEqual(presenter.numberOfRows, 1)
    }
    
    func testChangeNewState() {
        presenter.getAllPosts()
        interactor.allPostList[0].isNew = true
        presenter.didSelectRow(at: IndexPath(row: 0, section: 0))
        XCTAssertEqual(interactor.allPostList[0].isNew, false)
    }
    
    func testDeleteAllItems() {
        presenter.getAllPosts()
        XCTAssertFalse(interactor.allPostList.isEmpty)
        presenter.deleteAllOnAction()
        XCTAssertTrue(interactor.allPostList.isEmpty)
    }
    
    func testDeletePost() {
        presenter.getAllPosts()
        let post = interactor.allPostList[0]
        presenter.deletePost(IndexPath(row: 0, section: 0))
        XCTAssertTrue(interactor.allPostList.filter { $0.id == post.id }.isEmpty)
    }
    
    func testDeleteFavoritePost() {
        presenter.getAllPosts()
        interactor.allPostList[0].isFavorite = true
        presenter.selectedSegment(MainSegmentedOptions.favorites.rawValue)
        let post = interactor.allPostList[0]
        presenter.deletePost(IndexPath(row: 0, section: 0))
        XCTAssertTrue(interactor.allPostList.filter { $0.id == post.id }.isEmpty)
    }
    
    func testRealoadPosts() {
        presenter.getAllPosts()
        XCTAssertFalse(interactor.allPostList.isEmpty)
        presenter.deleteAllOnAction()
        XCTAssertTrue(interactor.allPostList.isEmpty)
        presenter.didPressReload()
        XCTAssertFalse(interactor.allPostList.isEmpty)
    }
    
    func testDisplayDetail() {
        presenter.getAllPosts()
        presenter.didSelectRow(at: IndexPath(row: 0, section: 0))
        XCTAssertTrue(wireframe.showPostDetailCalled)
    }
}

extension MainPresenterTests {
    class TestMainInteractor: MainInteractorInterface {
        var allPostList: [Post] = []
        var favoritesList: [Post] { allPostList.filter { $0.isFavorite == true } }
        
        func getAllPosts(completion: @escaping (GetAllPostResult) -> Void) {
            guard let jsonMock = TestConstants.postResultMock.data(using: .utf8) else {
                completion(.error("data doesn't exits"))
                return
            }
            
            do {
                let result = try JSONDecoder().decode([Post].self, from: jsonMock)
                allPostList.append(contentsOf: result)
                completion(.success(result))
            } catch {
                completion(.error("data doesn't exits"))
            }
        }
        
        func updateStatus(of index: Int, isNew: Bool) {
            allPostList[index].isNew = isNew
        }
        
        func deleteAllPost() {
            allPostList.removeAll()
        }
        
        func deletePost(_ index: Int, section: MainSegmentedOptions) {
            switch section {
            case .all:
                allPostList.remove(at: index)
            case .favorites:
                let favoriteId = favoritesList[index].id
                guard let indexPost = allPostList.firstIndex(where: { $0.id == favoriteId }) else {
                    return
                }
                allPostList.remove(at: indexPost)
            }
        }
        
        func getPost(by index: Int, section: MainSegmentedOptions) -> Post? {
            switch section {
            case .all:
                return allPostList[index]
            case .favorites:
                return favoritesList[index]
            }
        }
    }
    
    class TestMainWireframe: MainWireframeInterface {
        var showPostDetailCalled: Bool = false
        func showPostDetail(_ post: Post, from view: MainViewInterface) {
            showPostDetailCalled = true
        }
    }
    
    class TestMainView: MainViewInterface {
        var isRealoadData: Bool = false
        var isLoader: Bool = false
        var emptyStateIsHidden: Bool = false
        var setUpEmptyState: Bool = false
        
        func changeLoader(isLoader: Bool) { self.isLoader = isLoader }
        func realoadData() { isRealoadData =  true }
        func emptyState(isHidden: Bool) { emptyStateIsHidden = isHidden }
        func setEmptyState(title: String, message: String) { setUpEmptyState = true }
    }
}
